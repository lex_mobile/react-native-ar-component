import THREE from "./three";
import { DeviceOrientation } from "react-native-device-orientation";

export const DeviceOrientationControls = function(
  obj,
  deviceNotSupportedCallback = () => {},
  deviceNotSupportedMsg = "Jūsų įrenginys nepalaiko papildytos realybės kameros funkcijų",
  onDeviceIsHeldPortrait = () => {}
) {
  this.obj = obj;
  this.obj.rotation.reorder("YXZ");

  this.enabled = true;

  this.deviceOrientation = {};
  this.screenOrientation = 0;
  this.mag$ = null;

  this.alphaOffset = 0; // radians

  const onDeviceOrientationChangeEvent = event => {
    this.deviceOrientation = event;
  };

  const onScreenOrientationChangeEvent = () => {
    this.screenOrientation = window.orientation || 0;
  };

  // The angles alpha, beta and gamma form a set of intrinsic Tait-Bryan angles of type Z-X'-Y''

  const setObjectQuaternion = (function() {
    const zzz = new THREE.Vector3(0, 0, 1);
    const euler = new THREE.Euler();
    const q0 = new THREE.Quaternion();
    const q1 = new THREE.Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5)); // - PI/2 around the x-axis

    return (quaternion, alpha = 0, beta = 0, gamma = 0, orient = 0) => {
      euler.set(beta, alpha, -gamma, "YXZ"); // 'ZXY' for the device, but 'YXZ' for us
      quaternion.setFromEuler(euler); // orient the device
      quaternion.multiply(q1); // camera looks out the back of the device, not the top
      quaternion.multiply(q0.setFromAxisAngle(zzz, -orient)); // adjust for screen orientation
    };
  })();

  this.connect = () => {
    onScreenOrientationChangeEvent(); // run once on load

    new DeviceOrientation()
      .then(obs => {
        if (!obs) return;
        this.deviceOrientation$ = obs;
        obs.subscribe(data => {
          onDeviceOrientationChangeEvent(data);
        });
      })
      .catch(() => {
        alert(deviceNotSupportedMsg);
        deviceNotSupportedCallback();
      });

    // window.addEventListener('orientationchange', onScreenOrientationChangeEvent, false);
    // window.addEventListener('deviceorientation', onDeviceOrientationChangeEvent, false);

    this.enabled = true;
  };

  this.disconnect = () => {
    if (this.deviceOrientation$) {
      this.deviceOrientation$.stop();
      this.deviceOrientation$ = undefined;
    }

    this.enabled = false;
  };

  this.update = () => {
    if (this.enabled === false) return;

    const device = this.deviceOrientation;
    if (device) {
      const deviceIsHeldPortrait = Math.abs(90 - device.beta) < 25;

      if (this._deviceIsHeldPortrait !== deviceIsHeldPortrait) {
        this._deviceIsHeldPortrait = deviceIsHeldPortrait;
        onDeviceIsHeldPortrait(deviceIsHeldPortrait);
      }

      const alpha = device.alpha
        ? THREE.Math.degToRad(device.alpha) + this.alphaOffset
        : 0; // Z
      const beta = device.beta ? THREE.Math.degToRad(device.beta) : 0; // X'
      const gamma = device.gamma ? THREE.Math.degToRad(device.gamma) : 0; // Y''
      const orient = this.screenOrientation
        ? THREE.Math.degToRad(this.screenOrientation)
        : 0; // O
      setObjectQuaternion(this.obj.quaternion, alpha, beta, gamma, orient);
    }
  };

  this.dispose = () => {
    this.disconnect();
  };

  this.connect();
};

export default DeviceOrientationControls;
