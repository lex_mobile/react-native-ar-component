/**
 * @flow
 */

import React, { Component } from "react";
import { Dimensions, StyleSheet, Text, View } from "react-native";
import { RNCamera } from "react-native-camera";
import { WebGLView } from "react-native-webgl";
import { throttle } from "lodash";
import DeviceOrientationControls from "./three/DeviceOrientationControls";
import THREE from "./three/three";
import { calcDistances, LatLngToXY } from "./utilities";

const FONT = require("./font.json");

export type LatLng = {
  lat: number,
  lng: number
};

type Place = LatLng & {
  [x: string]: any
};

export type ARProps = {
  places: Place[],
  location: LatLng,
  onIntersect: (places: Place[]) => void,
  onDeviceNotSupported: () => void
};

const COORD_AXES = [1, 1, -1];

export default class ArComponent extends Component<ARProps> {
  requestId: *;
  WORLD_SCALE: number;
  OBJECT_SCALE: number;
  placeGroups: any[];
  _INTERSECTED: any[];
  _watchId: number | void;
  controls: any;
  scene: any;

  constructor(props) {
    super(props);

    this.placeGroups = [];
    this._INTERSECTED = [];
    this.WORLD_SCALE = 0.1;
    this.OBJECT_SCALE = 1;

    this.onContextCreate = this.onContextCreate.bind(this);
    this.onIntersectDelayed = throttle(this.onIntersect, 500);
  }

  componentDidMount() {}

  updatePlacesPosition(location = this.props.location) {
    for (let i = 0; i < this.placeGroups.length; i++) {
      const { lat, lng } = this.placeGroups[i].userData;
      const xy = LatLngToXY({ lat, lng }, location);

      // Update distance data
      this.placeGroups[i].userData = {
        ...this.placeGroups[i].userData,
        ...xy
      };

      // Hide if very close
      this.placeGroups[i].visible = xy.distance > 17.5;
      this.placeGroups[i].position.set(
        COORD_AXES[0] * xy.x * this.WORLD_SCALE,
        COORD_AXES[1] * 0,
        COORD_AXES[2] * xy.y * this.WORLD_SCALE
      );
      this.placeGroups[i].scale.set(
        this.OBJECT_SCALE,
        this.OBJECT_SCALE,
        this.OBJECT_SCALE
      );
    }
  }

  shouldComponentUpdate({ location }) {
    this.updatePlacesPosition(location);
    return true;
  }

  setWorldScale(value) {
    this.WORLD_SCALE = value;
    this.updatePlacesPosition();
  }
  setObjectScale(value) {
    this.OBJECT_SCALE = value;
    this.updatePlacesPosition();
  }

  componentWillUnmount() {
    this.controls && this.controls.disconnect();
  }

  onIntersect(intersected, objects) {
    // Callback, passed via props
    const { onIntersect = () => {} } = this.props;

    let colored = 0;
    const sorted = objects.sort(
      (a, b) => a.screenSpaceLength - b.screenSpaceLength
    );
    /*
    const sortedIntersects = intersected.sort(
      (a, b) => a.screenSpaceLength - b.screenSpaceLength
    );
    */
    const intersectsToSend = [];
    const closeObjects = [];

    for (let i = 0; i < sorted.length; i++) {
      const obj = sorted[i];
      const place = obj.children[0];
      const text = obj.children[1];
      const line = obj.children[2];

      // Object
      place.material.color.set(obj.currentColor);
      text.material.color.set(obj.highlightColor);
      text.visible = false;
      line.visible = false;

      if (obj.visible === false) {
        closeObjects.push(obj.userData);
      }

      if (obj.isLookedAt && colored < 5) {
        colored++;

        // Object
        place.material.color.set(obj.highlightColor);
        // Text
        text.visible = true;
        // Line
        line.visible = true;

        intersectsToSend.push(obj.userData);
      }
    }

    onIntersect(intersectsToSend, closeObjects);
  }

  onContextCreate(gl: WebGLRenderingContext) {
    const { WORLD_SCALE } = this;
    const rngl = gl.getExtension("RN");
    const {
      location,
      places: data,
      lang,
      onDeviceNotSupported = () => {},
      onDeviceIsHeldPortrait = () => {}
    } = this.props;

    const { drawingBufferWidth: width, drawingBufferHeight: height } = gl;

    const renderer = new THREE.WebGLRenderer({
      alpha: true,
      canvas: {
        width: width,
        height: height,

        style: {},
        addEventListener: () => {},
        removeEventListener: () => {},
        clientHeight: height
      },
      context: gl
    });
    renderer.setSize(width, height);

    let camera, scene, controls, fontLoader, font;

    const unselect = () => {
      if (this._INTERSECTED.length !== 0) {
        for (let i = 0; i < this._INTERSECTED.length; i++) {
          this._INTERSECTED[i].children[0].material.color.set(
            this._INTERSECTED[i].currentHex
          );
        }

        forget();
      }
    };

    const forget = () => {
      this.onIntersectDelayed([], this.placeGroups);
      if (this._watchId !== undefined) {
        this._watchId = clearTimeout(this._watchId);
        this._INTERSECTED = [];
      }
    };

    const init = () => {
      camera = new THREE.PerspectiveCamera(75, width / height, 1, 1100);
      camera.position.set(0, 0, 0);
      scene = new THREE.Scene();
      controls = new DeviceOrientationControls(
        camera,
        onDeviceNotSupported,
        lang.deviceNotSupported,
        onDeviceIsHeldPortrait
      );

      fontLoader = new THREE.FontLoader();
      font = fontLoader.parse(FONT);

      const addHorizonLine = () => {
        const curve = new THREE.EllipseCurve(
          0, // ox, oY
          0,
          50, // xRadius, yRadius
          50,
          0, // aStartAngle, aEndAngle
          2 * Math.PI,
          false, // aClockwise
          0 // aRotation
        );

        const points = curve.getPoints(50);
        const geometry = new THREE.BufferGeometry().setFromPoints(points);

        geometry.rotateX(Math.PI / 2);

        const material = new THREE.LineBasicMaterial({
          color: 0xffffff,
          opacity: 0.5,
          linewidth: 4
        });

        // Create the final object to add to the scene
        const ellipse = new THREE.Line(geometry, material);

        scene.add(ellipse);
      };

      const createPlaceMesh = place => {
        const colorValue = place.TypeId === 7 ? 0x2e7d32 : 0x774946;
        const color = new THREE.Color(colorValue);
        color.offsetHSL(
          (Math.random() - 0.5) / 3,
          (Math.random() / 100) * 20,
          (Math.random() / 100) * 8
        );

        place.color = "#" + color.getHexString();

        const geometries = [
          new THREE.SphereBufferGeometry(1, 8, 8),
          new THREE.OctahedronBufferGeometry(1)
        ];
        const geometry = place.TypeId === 7 ? geometries[0] : geometries[1];
        const material = new THREE.MeshBasicMaterial({ color });
        const mesh = new THREE.Mesh(geometry, material);

        return mesh;
      };

      const createPlaceText = place => {
        const color = new THREE.Color(0xffffff);
        const geometry = new THREE.TextGeometry(place.ObjectNumber, {
          font: font,
          size: 1,
          height: 1 / 5,
          curveSegments: 1,
          bevelEnabled: false
        }).center();

        const material = new THREE.MeshBasicMaterial({ color });
        const mesh = new THREE.Mesh(geometry, material);

        mesh.position.setY(2);
        mesh.visible = false;

        lookAtCamera(mesh);

        return mesh;
      };

      const createPlaceLine = place => {
        const material = new THREE.LineBasicMaterial({
          color: 0xffffff,
          linewidth: 5,
          linecap: "round"
        });

        const geometry = new THREE.Geometry();
        geometry.vertices.push(
          new THREE.Vector3(0, 0, 0),
          new THREE.Vector3(0, 1.3, 0)
        );

        const line = new THREE.Line(geometry, material);

        line.visible = false;

        return line;
      };

      const addPlaces = () => {
        /**
         * Add all place objects to Scene
         */
        const places = calcDistances(data, location);

        places.forEach(({ xyz, ...userData }, i) => {
          const group = new THREE.Group();
          const placeMesh = createPlaceMesh(userData);
          const placeText = createPlaceText(userData);
          const placeLine = createPlaceLine(userData);

          group.add(placeMesh, placeText, placeLine);

          group.position.set(
            COORD_AXES[0] * xyz[0] * WORLD_SCALE,
            COORD_AXES[1] * xyz[1] * WORLD_SCALE,
            COORD_AXES[2] * xyz[2] * WORLD_SCALE
          );

          group.userData = userData;
          group.currentColor = placeMesh.material.color.clone();
          group.highlightColor = group.currentColor
            .clone()
            .offsetHSL(0.05, 0.1, 0.2);

          scene.add(group);
          this.placeGroups.push(group);
        });
      };

      addHorizonLine();
      addPlaces();

      this.controls = controls;
    };

    const lookAtCamera = obj => obj.quaternion.copy(camera.quaternion);

    const update = () => {
      for (let i = 0; i < this.placeGroups.length; i++) {
        lookAtCamera(this.placeGroups[i].children[0]);
        lookAtCamera(this.placeGroups[i].children[1]);
      }
    };

    const updateDelayed = throttle(update, 500);

    const animate = () => {
      setTimeout(() => {
        findIntersectionsDelayed();
        renderer.render(scene, camera);
        this.requestId = requestAnimationFrame(animate);
        controls.update();
        updateDelayed();

        gl.flush();
        rngl.endFrame();
      }, 42); // 24 fps
    };

    const findIntersections = () => {
      let intersected = [];

      // Camera frustrum
      camera.updateMatrix();
      camera.updateMatrixWorld();
      var frustum = new THREE.Frustum();
      frustum.setFromMatrix(
        new THREE.Matrix4().multiplyMatrices(
          camera.projectionMatrix,
          camera.matrixWorldInverse
        )
      );

      // 1. Find close objects
      for (let i = 0; i < this.placeGroups.length; i++) {
        const obj = this.placeGroups[i];

        // if (obj.visible === false) return;

        const threshold = 0.8;
        const positionScreenSpace = obj.position.clone().project(camera);

        positionScreenSpace.setZ(0);
        obj.screenSpaceLength = positionScreenSpace.length();

        const isNearCenterOfScreen = obj.screenSpaceLength < threshold;
        const isInView = frustum.containsPoint(obj.position);

        obj.isLookedAt = isInView === true;

        if (obj.isLookedAt) {
          intersected.push(obj);
        }
      }

      intersected = intersected.reverse();

      // 2. Filter new ones
      if (intersected.length !== 0) {
        let isDiff;

        isDiff = intersected.length !== this._INTERSECTED.length;

        if (isDiff === false) {
          for (let i = 0; i < intersected.length; i++) {
            if (intersected[i] !== this._INTERSECTED[i]) {
              isDiff = true;
              break;
            }
          }
        }

        if (isDiff === true) {
          this._INTERSECTED = intersected;
          this.onIntersectDelayed(this._INTERSECTED, this.placeGroups);
        }
      } else {
        // No intersection
        unselect();
      }
    };

    const findIntersectionsDelayed = throttle(findIntersections, 1000);

    init();
    animate();
  }

  render() {
    const { lang } = this.props;
    return (
      <View style={styles.container}>
        <RNCamera
          type={RNCamera.Constants.Type.back}
          permissionDialogTitle={lang.cameraPermissionsTitle}
          permissionDialogMessage={lang.cameraPermissionsDescription}
          style={styles.webglView}
        />
        <WebGLView
          style={styles.webglView}
          onContextCreate={this.onContextCreate}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  webglView: {
    height: Dimensions.get("screen").height,
    width: Dimensions.get("screen").width,
    position: "absolute",
    top: 0,
    left: 0
  },
  container: {
    backgroundColor: "#707070",
    width: Dimensions.get("screen").width,
    height: Dimensions.get("screen").height
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
