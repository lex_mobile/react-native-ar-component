export const LatLngToXY = (target, origin) => {
  // Aliases
  const o = origin,
    t = target;
  // Magic
  const angle = 0;
  // Išvestiniai dydžiai
  let x, y, r;

  x = (t.lng - o.lng) * METERS_LONGITUDE(o.lat);
  y = (t.lat - o.lat) * METERS_LATITUDE(o.lat);

  r = Math.sqrt(x * x + y * y);

  if (r) {
    let ct, st;
    ct = x / r;
    st = y / r;
    x = r * (ct * Math.cos(angle) + st * Math.sin(angle));
    y = r * (st * Math.cos(angle) - ct * Math.sin(angle));
  }

  const distance = Math.sqrt(x * x + y * y);
  const distanceText = humanDistance(distance);

  return { x, y, distance, distanceText };
};
export const METERS_LONGITUDE = degrees => {
  const r = toRadians(degrees);
  return (
    111415.13 * Math.cos(r) -
    94.55 * Math.cos(3.0 * r) +
    0.12 * Math.cos(5.0 * r)
  );
};

export const METERS_LATITUDE = degrees => {
  const r = toRadians(degrees);
  return (
    111132.09 -
    566.05 * Math.cos(2.0 * r) +
    1.2 * Math.cos(4.0 * r) -
    0.002 * Math.cos(6.0 * r)
  );
};

export const hipotenuse = (a, b) => Math.sqrt(a * a + b * b);

export const calcDistances = (data, ref) => {
  return data
    .map(d => {
      const { Latitude: lat, Longitude: lng } = d;
      const latlng = {
        lat,
        lng
      };

      const xy = LatLngToXY(latlng, ref);
      const distance = hipotenuse(xy.x, xy.y);
      const distanceText = humanDistance(distance);
      const xyz = [-xy.x, 0, xy.y];

      return {
        ...latlng,
        ...d,
        distance,
        distanceText,
        xyz
      };
    })
    .sort((a, b) => a.distance - b.distance);
};

export const EARTH_RADIUS_IN_METERS = 6371e3;

export const toDegrees = radians => (radians * 180) / Math.PI;
export const toRadians = degrees => (degrees * Math.PI) / 180;

/**
 * Paskaičiuoja atstumą metrais tarp dviejų koordinačių (c2 - c1), kurios paduotos objekte.
 * Formatas: LatLng = {lat: number, }
 * @param {LatLng} a
 * @param {LatLng} b
 * @return {number} atstumas metrais
 */
export const coordDistance = (a, b) => {
  if (!a || !b) return 0;

  const dlon = b.lng - a.lng;
  const dlat = b.lat - a.lat;
  const ar =
    Math.pow(Math.sin(dlat / 2), 2) +
    Math.cos(a.lat) * Math.cos(b.lat) * Math.pow(Math.sin(dlon / 2), 2);

  const c = 2 * Math.atan2(Math.sqrt(ar), Math.sqrt(1 - ar));
  return (EARTH_RADIUS_IN_METERS / 1000) * c;
};

/**
 * Suformatuoją metrus į human readable formatą.
 * @param {number} meters
 */
export const humanDistance = meters => {
  if (!meters) return "";
  const km = Math.floor(meters / 1000);

  if (km) {
    const p = 100;
    return Math.round((meters * p) / 1000) / p + " km";
  }

  return Math.round(meters) + " m";
};
