import React from "react";
import { AppRegistry, View, Text, Slider, StyleSheet } from "react-native";
import AR, { ARProps } from "./src/AR";
import { DeviceOrientation } from "react-native-device-orientation";

type ArComponentProps = ARProps & {
  debug: boolean
};
type ArComponentState = {
  hasOrientationSensors: boolean,
  loading: boolean
};

const Message = ({ text }) => (
  <View style={styles.messageContainer}>
    <Text style={styles.message}>{text}</Text>
  </View>
);

export class ArComponent extends React.Component<
  ArComponentProps,
  ArComponentState
> {
  constructor(props) {
    super(props);

    this.state = {
      hasOrientationSensors: null,
      loading: true
    };
  }

  componentDidMount() {
    const { debug } = this.props;

    const updateOrientationState = (hasOrientationSensors, observable) => {
      // Update internal state if device can detect orientation
      if (observable) observable.stop();
      setTimeout(
        () => this.setState({ loading: false, hasOrientationSensors }),
        0
      );
    };

    // Check if device is capable of detecting device orientation
    new DeviceOrientation()
      .then(obs => updateOrientationState(true, obs))
      .catch(() => updateOrientationState(false));

    if (debug) {
      // Mock location updates if debugging
      setTimeout(() => {
        setInterval(() => {
          this.setState({
            ...this.state,
            location: {
              lat: this.state.location.lat,
              lng: this.state.location.lng + 0.00001
            }
          });
        }, 500);
      }, 3000);
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.places !== nextState.places || // Got new places
      this.state.hasOrientationSensors !== nextState.hasOrientationSensors || // Checked if device supports device orientation
      this.props.location !== nextProps.location // Got new location
    );
  }

  render() {
    const { places, lang, location } = this.props;
    const { loading, hasOrientationSensors } = this.state;

    return loading ? (
      <Message text={lang.loading} />
    ) : !hasOrientationSensors ? (
      <Message text={lang.deviceNotSupported} />
    ) : !location ? (
      <Message text={lang.locating} />
    ) : (
      <View>
        <AR ref={ref => (this.ARRef = ref)} {...this.props} />
        {/* <View style={styles.arControls}>
          <Text style={styles.arControlsText}>{lang.objectScale}</Text>
          <Slider
            minimumValue={0.001}
            maximumValue={5}
            value={1}
            onValueChange={value => this.ARRef.setObjectScale(value)}
          />
          <Text style={styles.arControlsText}>{lang.objectDistance}</Text>
          <Slider
            minimumValue={0.001}
            maximumValue={2}
            value={0.1}
            onValueChange={value => this.ARRef.setWorldScale(value)}
          />
        </View> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  arControls: {
    position: "absolute",
    width: "100%",
    backgroundColor: "rgba(0, 0, 0, .2)",
    paddingVertical: 12,
    paddingHorizontal: 16
  },
  arControlsText: {
    color: "#fff"
  },
  messageContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  message: {
    textAlign: "center"
  }
});

console.disableYellowBox = true;
AppRegistry.registerComponent("ArComponent", () => ArComponent);
